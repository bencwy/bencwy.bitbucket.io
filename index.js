(function(){
    var GiphyApp = angular.module("GiphyApp", []);

    var GiphyCtrl = function($http){
        var giphyCtrl = this;

        giphyCtrl.subject = "";
        giphyCtrl.image = "https://www.jennybeaumont.com/wp-content/uploads/2015/03/placeholder.gif";

        giphyCtrl.search = function(){
            if (!giphyCtrl.subject || (giphyCtrl.subject.trim().length <= 0))
                return
            console.log("in search: %s", giphyCtrl.subject);
            var promise = $http.get("https://api.giphy.com/v1/gifs/random",{
                params:{
                    api_key: "dc6zaTOxFJmzC",
                    tag: giphyCtrl.subject
                }
            });
            promise.then(function(result){
                console.log(result.data.data.image_url);
                giphyCtrl.image = result.data.data.image_url;
            });
        };

        // console.log(giphyCtrl.image);
    };

    GiphyCtrl.$inject = ["$http"];

    GiphyApp.controller("GiphyCtrl",GiphyCtrl);
})();